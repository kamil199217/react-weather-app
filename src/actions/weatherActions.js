export const addCity = (city) => {
    return (dispatch, getState, {getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        const profile  = getState().firebase.profile;
        const ownerId = getState().firebase.auth.uid;
        firestore.collection('cities').add({
            ...city,
            userEmail: profile.email,
            ownerId: ownerId,
            createdAt: new Date()
        }).then(()=>{
            dispatch({type: 'ADD_CITY', city});
        }).catch((err)=>{
            dispatch({type: 'ADD_CITY_ERROR', err})
        })
    }
};
export const FETCH_WEATHER = 'FETCH_WEATHER';
export function fetchWeather(payload) {
    return {
        type: 'FETCH_WEATHER',
        payload
    }
}
