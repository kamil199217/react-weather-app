import React from 'react';
import './CitySummary.css';
import {fetchWeather} from "../../../common/axios-instances";


class CitySummary extends React.Component {
    state = {
        desc: '',
        temp: '',
        icon: '',
    };

    componentDidMount() {
        this.mounted = true;
        fetchWeather(this.props.name).then(res => {
            const {statusText, data} = res;
            if (statusText === 'OK' && this.mounted) {
                this.setState({temp: data.main.temp, desc: data.weather[0].description, icon: data.weather[0].icon})
            }
        });
    }

    componentWillUnmount() {
        this.mounted = false;
    }


    render() {
        const {icon, desc, temp} = this.state;
        const {name, country} = this.props;
        return (
            <div className="section city-details">
                <div className="card-action white-text weather-conditions">
                    <div className="row">
                        <div className="col s6">
                            <div className="weather-city">{name}, {country} </div>
                            {icon && (
                                <img src={`http://openweathermap.org/img/w/${icon}.png`} alt='icon'/>
                            )}
                        </div>
                        {temp && (
                            <div className="col s6 weather-info">
                                <div className="weather-temperature">{temp}<sup>o</sup>C</div>
                                <div className="weather-description">{desc}</div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        )
    }
}


export default CitySummary;
