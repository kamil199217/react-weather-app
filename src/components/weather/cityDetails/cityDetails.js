import React from 'react';
import './cityDetails.css';
import {connect} from 'react-redux';
import {firestoreConnect} from "react-redux-firebase";
import {compose} from 'redux';
import {fetchWeather} from "../../../common/axios-instances";
import {Redirect} from "react-router-dom";

class CityDetails extends React.Component {
    state = {
        minTemp: '',
        maxTemp: '',
        feelTemp: '',
        cloud: '',
        humidity: '',
        wind: '',
        rain: '',
        snow : '',
        pressure: '',
        temp: '',
        desc: '',
        icon: ''
    };

    componentDidMount() {
        fetchWeather(this.props.city.name).then(res => {
            const {statusText, data} = res;
            if (statusText === 'OK') {
                this.setState({
                    minTemp: data.main.temp_min,
                    maxTemp: data.main.temp_max,
                    feelTemp: data.main.feels_like,
                    cloud: data.clouds.all,
                    humidity: data.main.humidity,
                    wind: data.wind.speed,
                    pressure: data.main.pressure,
                    temp: data.main.temp,
                    desc: data.weather[0].description,
                    rain: data.rain ? data.rain['1h'] : null,
                    snow: data.snow ? data.snow['1h'] : null,
                    icon: data.weather[0].icon
                })
            }
        });
    }

    render(){
        const {city, auth} = this.props;
        const {minTemp, maxTemp, feelTemp, cloud, humidity, wind, pressure, temp, desc, icon, rain, snow} = this.state;
        if(!auth.uid){
            return <Redirect to='/signin' />
        }
        return (
            <div className="container section city-details">
                <div className="card-action white-text weather-conditions">
                    <div className="row">
                        <div className="col s6">
                            <div className="weather-city">{city.name}, {city.country} </div>
                            {icon && (
                                <img src={`http://openweathermap.org/img/w/${icon}.png`} alt='icon'/>
                            )}
                        </div>
                        <div className="col s6 weather-info">
                            <div className="weather-temperature">{temp}<sup>o</sup>C</div>
                            <div className="weather-description">{desc}</div>
                        </div>
                    </div>
                    <div className="row weather-more-info">
                        <div className="col s3 weather-condition-desc">
                            <div className="weather-key">Min temp.</div>
                            <div className="weather-value">{minTemp}<sup>o</sup>C</div>
                        </div>
                        <div className="col s3 weather-condition-desc">
                            <div className="weather-key" >Max temp.</div>
                            <div className="weather-value">{maxTemp}<sup>o</sup>C</div>
                        </div>
                        <div className="col s3 weather-condition-desc">
                            <div className="weather-key">Feeling temp.</div>
                            <div className="weather-value">{feelTemp}<sup>o</sup>C</div>
                        </div>
                        <div className="col s3 weather-condition-desc">
                            <div className="weather-key">Cloudiness</div>
                            <div className="weather-value">{cloud}%</div>
                        </div>
                        <div className="col s3 weather-condition-desc">
                            <div className="weather-key">Humidity</div>
                            <div className="weather-value">{humidity}%</div>
                        </div>
                        <div className="col s3 weather-condition-desc">
                            <div className="weather-key">Wind</div>
                            <div className="weather-value">{wind}km/h</div>
                        </div>
                        {rain && (
                        <div className="col s3 weather-condition-desc">
                            <div className="weather-key">Rain</div>
                            <div className="weather-value">{rain}mm/h</div>
                        </div>
                        )}
                        {snow && (
                            <div className="col s3 weather-condition-desc">
                                <div className="weather-key">Snow</div>
                                <div className="weather-value">{snow}mm/h</div>
                            </div>
                        )}
                        <div className="col s3 weather-condition-desc">
                            <div className="weather-key">Pressure</div>
                            <div className="weather-value">{pressure}hPa</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id;
    const cities = state.firestore.data.cities;
    const city = cities ? cities[id] : null;
    return {
        city: city,
        auth: state.firebase.auth
    }
};

export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        {collection: 'cities'}
    ])
)(CityDetails);
