import React from "react";
import CitySummary from "../citySummary/CitySummary";
import {Link} from "react-router-dom";
import Loader from "../../shared/loader/Loader";

const CityList = ({cities}) => {

    if (cities) {
        return (
            <div className="project-list section">
                {cities && cities.map(city => {
                        return (
                            <Link to={'/city/' + city.id }  key={city.id}>
                                <CitySummary name={city.name} country={city.country}/>
                            </Link>

                        )
                    }
                )}
            </div>
        )
    } else {
        return (
            <Loader/>
        );
    }

};
export default CityList;
