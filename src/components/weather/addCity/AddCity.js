import React from 'react'
import {connect} from 'react-redux';
import {addCity} from "../../../actions/weatherActions";
import {fetchWeather} from "../../../common/axios-instances";
import CitySummary from "../citySummary/CitySummary";
import {Redirect} from "react-router-dom";

class AddCity extends React.Component {
    state = {
        name: '',
        country: '',
        desc: '',
        temp: '',
        icon: '',
        searchCity: ''
    };

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        });

    };

    searchCity = (e) => {
        e.preventDefault();
        fetchWeather(this.state.name).then(res => {
            const {statusText, data} = res;
            if (statusText === 'OK') {
                this.setState({
                    searchCity: this.state.name,
                    country: data.sys.country,
                    desc: data.weather[0].description,
                    temp: data.main.temp,
                    icon: data.weather[0].icon
                });
            }
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const city = {
            name: this.state.name,
            country: this.state.country
        };
        this.props.addCity(city);
        this.props.history.push('/')
    };

    render() {
        const {searchCity, country} = this.state;
        const {auth} =  this.props;
        if(!auth.uid){
            return <Redirect to='/signin' />
        }
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit} className="white">
                    <h5 className="grey-text text-darken-3">Add City</h5>
                    <div className="input-field">
                        <label htmlFor="name">City</label>
                        <input type="text" id="name" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <button className="btn pink lighten-1 z-depth-0" onClick={this.searchCity}>Search city</button>
                    </div>
                    {this.state.temp && (
                        <div>
                            <CitySummary name={searchCity} country={country}/>
                            <div className="input-field">
                                <button className="btn pink lighten-1 z-depth-0">Add to observed</button>
                            </div>
                        </div>
                    )}

                </form>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addCity: (city) => dispatch(addCity(city))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCity);
