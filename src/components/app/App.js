import React from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Navbar from "../shared/navbar/Navbar";
import CityDetails from "../weather/cityDetails/cityDetails";
import SignIn from "../auth/signIn/SignIn";
import SignUp from "../auth/signUp/SignUp";
import AddCity from "../weather/addCity/AddCity";
import Dashboard from "../dashboard/Dashboard";


function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Navbar/>
                <Switch>
                    <Route exact path='/' component={Dashboard}/>
                    <Route path='/city/:id' component={CityDetails} />
                    <Route path='/signin' component={SignIn} />
                    <Route path='/signup' component={SignUp} />
                    <Route path='/addcity' component={AddCity} />
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;
