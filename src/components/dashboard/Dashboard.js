import React, {Component} from "react";
import CityList from "../weather/cityList/CityList";
import {connect} from 'react-redux';
import {firestoreConnect} from "react-redux-firebase";
import {compose} from "redux";
import {Redirect} from 'react-router-dom'

class Dashboard extends Component {
    render() {
        const {cities, auth} =  this.props;
        if(!auth.uid){
            return <Redirect to='/signin' />
        }
            return (
                <div className="dashboard container">
                    <div className="row">
                        <div className="col s12">
                            <CityList cities={cities}/>
                        </div>
                    </div>
                </div>
            )
    }
}

const mapStateToProps = (state) => {
    return {
        cities: state.firestore.ordered.cities,
        auth: state.firebase.auth,
        profile: state.firebase.profile
    }
};

export default compose(
    connect(mapStateToProps),
    firestoreConnect(props => [
        {
            collection: 'cities',
            where: [['ownerId', '==',  props.auth.uid || null]]
        },
    ])
)(Dashboard);
