import React from "react";
import {NavLink} from "react-router-dom";
import {connect} from 'react-redux';
import {signOut} from "../../../actions/authActions";
import { useHistory } from "react-router-dom";



const SignedInLinks = (props) => {
    let history = useHistory();
    const logout = () =>{
        props.signOut();
        history.push('/');
    };

    return (
        <ul className="right">
            <li><NavLink to='/addcity'>Add city</NavLink></li>
            <li><a onClick={logout}>Logout</a></li>
            <li><NavLink to='/' className="btn btn-floating pink lighten-1">{props.profile.initials}</NavLink></li>
        </ul>
    )
};

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: () => dispatch(signOut())
    }
};

export default connect(null, mapDispatchToProps)(SignedInLinks);
