import axios, {CancelToken} from 'axios';
import {API_BASE_URL, WEATHER} from "./uri";

const KEY = '070769a5819435191e636ba94f960825';
const mainAxiosInstance = axios.create({
    baseURL: API_BASE_URL,
    headers: {'content-type': 'application/json'}
});

export const sourceCancelTokenWeather = CancelToken.source();
export const fetchWeather = (city) => mainAxiosInstance.request({
    method: 'get',
    url: WEATHER + `?q=${city}&units=metric&appid=${KEY}`,
    cancelToken: sourceCancelTokenWeather.token
});
