import firebase from "firebase/app";
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyC_fPPPQaw3FpLwpzNdKFy1vh1aUP0IGgI",
    authDomain: "react-weather-acb4b.firebaseapp.com",
    databaseURL: "https://react-weather-acb4b.firebaseio.com",
    projectId: "react-weather-acb4b",
    storageBucket: "react-weather-acb4b.appspot.com",
    messagingSenderId: "615868049314",
    appId: "1:615868049314:web:decf1b34fd3455accfb4fb"
};
firebase.initializeApp(config);
firebase.firestore().settings({});

export default firebase;

