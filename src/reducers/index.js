import {combineReducers} from "redux";
import authReducer from "./authReducer";
import weatherReducer from "./weatherReducer";
import {firestoreReducer} from "redux-firestore";
import {firebaseReducer} from "react-redux-firebase";

const rootReducer = combineReducers({
    auth: authReducer,
    weather: weatherReducer,
    firestore: firestoreReducer,
    firebase: firebaseReducer
});
export default rootReducer;
