const initState = {
    weather: null
};

const weatherReducer = (state = initState, action) => {
    switch (action.type) {
        case 'ADD_CITY':
            return state;
        case 'ADD_CITY_ERROR':
            return state;
        case 'FETCH_WEATHER':
            return {
                ...state,
                weather: action.payload
            };
        default:
            return state;
    }
};

export default weatherReducer;
